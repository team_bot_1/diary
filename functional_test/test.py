from django.test import LiveServerTestCase
from selenium.webdriver import Chrome
from diary import settings as st


class NewVisitorTest(LiveServerTestCase):

    def setUp(self):
        self.browser = Chrome(st.CHROME_DRIVER_PATH)
        self.browser.get(st.BASE_URL)
        self.browser.implicitly_wait(st.IMPLICIT_WAIT)

    def test_title_is_set_correctly(self):
        self.assertIn('Django', self.browser.title)
        self.fail('Nothing to test')

    def tearDown(self):
        self.browser.quit()
